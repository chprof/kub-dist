document.addEventListener("DOMContentLoaded", () => {
  svg4everybody();
	

	var forms = document.querySelectorAll('.needs-validation');  
	var validation = Array.prototype.filter.call(forms, function(form) {
		form.addEventListener('submit', function(event) {
			if (form.checkValidity() === false) {
				event.preventDefault();
				event.stopPropagation();
			}
			form.classList.add('was-validated');
		})
	});
	
	
	var rates = document.querySelectorAll('[data-element="form-rate"]');  
	var rate = Array.prototype.filter.call(rates, function(rate) {
		var rateInput = $(rate).find('input');
		var ratePseudo = $(rate).find('[data-element="rate-pseudo"]');
		$(ratePseudo).rateit({ max: 5, step: 1, backingfld: `#${$(rateInput).attr('id')}` });
	});



	// (function() {
	// 	var togglePasswords = $('[data-element="password-toggle"]');
	// 	togglePasswords.each(function(i, toggle) {
	// 		var input = $(toggle).parent().find('input');
	// 		$(toggle).on('click', function(e) {
	// 			if(input.attr('type') == 'text') {
	// 				input.attr('type', 'password');
	// 			} else if(input.attr('type') == 'password') {
	// 				input.attr('type', 'text');
	// 			}
	// 		})
	// 	})
	// })();




	
		var toggles = document.querySelectorAll('[data-element="toggle"]');
		Array.prototype.filter.call(toggles, function(toggle) {
			toggle.addEventListener('click', function(e) {
				var _targetSelector = e.currentTarget.dataset.target;
				console.log(_targetSelector);
				document.querySelector(_targetSelector).classList.toggle('visible');
				if (document.querySelector(_targetSelector).classList.contains('visible') && (_targetSelector !== '#dropdown' && _targetSelector !== '#dropdown-mobile')) {
					document.body.style.overflow = 'hidden';
				} else {
					document.body.style.overflow = 'visible';
				}
			})
		})


	

	var $rangeComponents = $('[data-element="range-slider-component"]');
	
	$rangeComponents.each(function(i, el) {
		var $range = $(el).find("[data-element='range-slider']"),
			$inputFrom = $(el).find("[data-element='range-input-min']"),
			$inputTo = $(el).find("[data-element='range-input-max']"),
			instance,
			min = $inputFrom.data('min'),
			max = $inputTo.data('max'),
			from = 0,
			to = 0;
			
			$range.ionRangeSlider({
				skin: "round",
					type: "double",
					min: min,
					max: max,
					from: 200,
					to: 800,
					onStart: updateInputs,
					onChange: updateInputs
			});
			instance = $range.data("ionRangeSlider");
		
			function updateInputs (data) {
				from = data.from;
					to = data.to;
					
					$inputFrom.prop("value", from);
					$inputTo.prop("value", to);	
			}
		
			$inputFrom.on("input", function () {
					var val = $(this).prop("value");
					
					// validate
					if (val < min) {
							val = min;
					} else if (val > to) {
							val = to;
					}
					
					instance.update({
							from: val
					});
			});
		
			$inputTo.on("input", function () {
					var val = $(this).prop("value");
					
					// validate
					if (val < from) {
							val = from;
					} else if (val > max) {
							val = max;
					}
					
					instance.update({
							to: val
					});
			});
		})



	
		var carousel = new Swiper("[data-element='carousel']", {
			slidesPerView: 'auto',
			navigation: {
				nextEl: ".swiper-button-next",
				prevEl: ".swiper-button-prev",
			},
			loop: true,
			grabCursor: true,
			effect: "creative",
			creativeEffect: {
				prev: {
					shadow: true,
					translate: [0, 0, -400],
				},
				next: {
					translate: ["100%", 0, 0],
				},
			},
			autoplay: {
				delay: 5000,
			},
			speed: 1000,
		});


		$('[data-element="magnific-gallery"]').each(function () {
			$(this).magnificPopup({
				delegate: 'a',
				type: 'image',
				gallery: {
					enabled: true
				},
				mainClass: 'mfp-with-zoom',
				zoom: {
					enabled: true,
					duration: 300,
					easing: 'ease-in-out',
					opener: function(openerElement) {
						return openerElement.is('img') ? openerElement : openerElement.find('img');
					}
				}
			});
		})
	
	

});